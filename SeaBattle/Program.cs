﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SeaBattle
{ 
    public enum Status
        { 
            unnown,
            missed,
            wounded,
            killed,
            wins,
            ships_aureole
        }
    public struct Point
    {
        public int x;
        public int y;


        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]

        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Battleship());
        }
    }
}
