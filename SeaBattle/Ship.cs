﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeaBattle
{
    class Ship
    {
        public Point[] deck { get; private set;}  //массив с палубами корабля
        int hits;

        public Ship(Point[] deck) {
            this.deck = deck;
            this.hits = 0;
        }

        public Status fire(Point p)   //функция выстрела
        {
            for (int i = 0; i <= deck.Length; i++ ) {
                if(deck[i].x == p.x && deck[i].y == p.y){
                    hits++;
                    if (hits == deck.Length) return Status.killed;
                    else return Status.wounded;
                }
            }
            return Status.missed;
        }
    }
}
