﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeaBattle
{
    public delegate void deleg_show_ship(Point p, int ships_number);
    public delegate void deleg_show_fire(Point p, Status status);
    public delegate void deleg_show_aureole(Point p, Status status);


    class Field
    {

        public deleg_show_ship show_ship;
        public deleg_show_fire show_fight;
        public deleg_show_aureole show_aureole;

        public static Point fieldSize = new Point(10, 10);
        public static int summaryShips = 10;    //всего кораблей

        public int[,] shipsMap; //-1 пусто, 0-9номера кораблей, которые остались
        public Status[,] hitsMap;// unnown,missed, wounded, killed, wins, ships_aureole

        public Ship[] ship;  //массив кораблей (каждый корабль- массивом палуб(точек)), счетчиком попаданий и методом выстрела по палубам

        public int setedShips{ get; protected set;}
        public int killedShips { get; protected set; }

        private List<Point> last_enemy_shoot = new List<Point>();
        

        public Field() { 
            shipsMap = new int[fieldSize.x, fieldSize.y];
            hitsMap = new Status[fieldSize.x, fieldSize.y];
            ship = new Ship[summaryShips];
        }


        Random rnd = new Random();
        
        public int onShipsMap(Point p) { //попали ли в карту кораблей
            if(onField(p)){
                return shipsMap[p.x,p.y];
            }
            return -1;
        }
        
        public Status onHitsMap(Point p) { //попали ли в карту выстрелов
            if(onField(p)){
                return hitsMap[p.x, p.y];
            }
            return Status.unnown;
        }

        public bool onField(Point p) {   //попали ли в поле
            return p.x >= 0 && p.x < fieldSize.x &&
                    p.y >= 0 && p.y < fieldSize.y;
        }

        public Status user_fire(Point p) { // выстрел
            if (!onField(p))
                return Status.unnown;
            if(hitsMap[p.x,p.y] != Status.unnown)
                return hitsMap[p.x, p.y];
            Status status;
            if (shipsMap[p.x, p.y] == -1)
            {
                hitsMap[p.x, p.y] = Status.missed;
                status = Status.missed;
            }
            else
            {
                status = ship[shipsMap[p.x, p.y]].fire(p);
            }
            hitsMap[p.x, p.y] = status;
            if (status == Status.killed) {
                killedShips++;
                set_aureol_around_ship(shipsMap[p.x, p.y]);
                if (killedShips >= setedShips)
                    status = Status.wins;
            }

            show_fight(p, status);
            return status;
       }


        public Status enemy_fire(Point p) // выстрел
        {
            if (!onField(p))
                return Status.unnown;
            if (hitsMap[p.x, p.y] != Status.unnown)
                return hitsMap[p.x, p.y];
            Status status;
            if (shipsMap[p.x, p.y] == -1)
            {
                hitsMap[p.x, p.y] = Status.missed;
                status = Status.missed;
            }
            else
            {
                status = ship[shipsMap[p.x, p.y]].fire(p);
            }
            hitsMap[p.x, p.y] = status;
            if (status == Status.killed)
            {
                killedShips++;
                set_aureol_around_ship(shipsMap[p.x, p.y]);
                last_enemy_shoot.Clear();
                if (killedShips >= setedShips)
                    status = Status.wins;
            }

            if (status == Status.wounded)
            {
                last_enemy_shoot.Add(p);
            }

            if (status == Status.wins)
            {
                
            }

            show_fight(p, status);
            return status;
        }

        protected void point_aureole(Point p)            //установить, что точка является ореолом
        {
            if (!onField(p))
                return;
            if (hitsMap[p.x, p.y] == Status.unnown)
            {
                hitsMap[p.x, p.y] = Status.ships_aureole;
                show_aureole(p, Status.ships_aureole);
            }
        }

        protected void set_aureol_around_point(Point p)          //обрамить точку под ореолом
        {
            Point innerP;

            for (innerP.x = p.x - 1; innerP.x <= p.x + 1; innerP.x++)
                for (innerP.y = p.y - 1; innerP.y <= p.y + 1; innerP.y++)
                    point_aureole(innerP);
        }

        protected void set_aureol_around_ship(int shipsNumber)
        { 
            Point[] decks = ship[shipsNumber].deck;
            foreach (Point deck in decks)
                set_aureol_around_point(deck);
        }

        public Point pointForFire()       // выстрел компьютера
        {
            Point p;
            Point[] probableShootPoints;
            bool[] possibleToShoot;

            if (last_enemy_shoot.Count == 0)                              // если нет раненого корабля
            {
                do
                {
                    p = new Point(rnd.Next(0, fieldSize.x), rnd.Next(0, fieldSize.y));
                }
                while (hitsMap[p.x, p.y] != Status.unnown);
                
                return p;


            }
            else if (last_enemy_shoot.Count == 1)                    //если ранена одна палуба
            {
                probableShootPoints = new Point[4];
                probableShootPoints[0] = new Point(last_enemy_shoot[0].x, last_enemy_shoot[0].y - 1);
                probableShootPoints[1] = new Point(last_enemy_shoot[0].x + 1, last_enemy_shoot[0].y);
                probableShootPoints[2] = new Point(last_enemy_shoot[0].x, last_enemy_shoot[0].y + 1);
                probableShootPoints[3] = new Point(last_enemy_shoot[0].x - 1, last_enemy_shoot[0].y);

                possibleToShoot = new bool[4];
            }
            else if(last_enemy_shoot.Count == 2)                     //если ранено две или три палубы
            {
                probableShootPoints = new Point[2];
                possibleToShoot = new bool[2];

                if (last_enemy_shoot[0].x == last_enemy_shoot[(last_enemy_shoot.Count) - 1].x)   //если корабль расположен по вертикали
                {
                    if (last_enemy_shoot[0].y < last_enemy_shoot[(last_enemy_shoot.Count) - 1].y)  //если первая раненная палуба по игрику меньше, чем последняя
                    {
                        probableShootPoints[0] = new Point(last_enemy_shoot[0].x, last_enemy_shoot[0].y - 1);
                        probableShootPoints[1] = new Point(last_enemy_shoot[0].x, last_enemy_shoot[last_enemy_shoot.Count-1].y + 1);
                    }
                    else   //если первая раненная палуба по игрику больше, чем последняя
                    {
                        probableShootPoints[0] = new Point(last_enemy_shoot[0].x, last_enemy_shoot[0].y + 1);
                        probableShootPoints[1] = new Point(last_enemy_shoot[0].x, last_enemy_shoot[last_enemy_shoot.Count - 1].y - 1);
                    }
                }
                else   //если корабль расположен по горизонтали
                {
                    if (last_enemy_shoot[0].x < last_enemy_shoot[(last_enemy_shoot.Count) - 1].x)  //если первая раненная палуба по иксу меньше, чем последняя
                    {
                        probableShootPoints[0] = new Point(last_enemy_shoot[0].x - 1, last_enemy_shoot[0].y);
                        probableShootPoints[1] = new Point(last_enemy_shoot[last_enemy_shoot.Count - 1].x +1, last_enemy_shoot[0].y);
                    }
                    else   //если первая раненная палуба по иксу больше, чем последняя
                    {
                        probableShootPoints[0] = new Point(last_enemy_shoot[0].x + 1, last_enemy_shoot[0].y);
                        probableShootPoints[1] = new Point(last_enemy_shoot[last_enemy_shoot.Count - 1].x - 1, last_enemy_shoot[0].y);
                    }
                }
            }
            else                     //если ранено три палубы
            {
                probableShootPoints = new Point[2];
                possibleToShoot = new bool[2];

                if (last_enemy_shoot[0].x == last_enemy_shoot[1].x)   //если корабль расположен по вертикали
                {
                    Point biggest = last_enemy_shoot[0];
                    Point smallest = last_enemy_shoot[0];
                    for (int i = 1; i < last_enemy_shoot.Count; i++ )
                    {
                        if (biggest.y < last_enemy_shoot[i].y)
                            biggest = last_enemy_shoot[i];

                        if (smallest.y > last_enemy_shoot[i].y)
                            smallest = last_enemy_shoot[i];
                    }
                    probableShootPoints[0] = new Point(smallest.x, smallest.y - 1);
                    probableShootPoints[1] = new Point(biggest.x, biggest.y + 1);
                }
                else   //если корабль расположен по горизонтали
                {
                    Point biggest = last_enemy_shoot[0];
                    Point smallest = last_enemy_shoot[0];
                    for (int i = 1; i < last_enemy_shoot.Count; i++)
                    {
                        if (biggest.x < last_enemy_shoot[i].x)
                            biggest = last_enemy_shoot[i];

                        if (smallest.x > last_enemy_shoot[i].x)
                            smallest = last_enemy_shoot[i];
                    }
                    probableShootPoints[0] = new Point(smallest.x - 1, smallest.y);
                    probableShootPoints[1] = new Point(biggest.x + 1, biggest.y);
                }
            }
            
            for (int i = 0; i < possibleToShoot.Length; i++)
            {
                if (!onField(probableShootPoints[i]))
                {
                    possibleToShoot[i] = false;
                }
                else
                {
                    switch (hitsMap[probableShootPoints[i].x, probableShootPoints[i].y])
                    {
                        case (Status.unnown): possibleToShoot[i] = true;
                            break;
                        default: possibleToShoot[i] = false;
                            break;
                    }
                }
            }
                
            while (true)
            {
                int randomPoint = rnd.Next(0, possibleToShoot.Length);
                if (possibleToShoot[randomPoint])
                {
                    p = probableShootPoints[randomPoint];
                    break;
                }
            }  
            return p;
        }
    }
}
