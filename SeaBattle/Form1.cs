﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SeaBattle
{
    public partial class Battleship : Form
    {
        Redactor user_field;
        Redactor enemy_field;

        static string abc = "абвгдежзик";

        Color field_color = Color.AliceBlue;
        Color[] ship_color = { Color.Blue,                                      //4палубный
                               Color.Blue, Color.Blue,                          //3палубные
                               Color.Blue, Color.Blue, Color.Blue,              //2палубные
                               Color.Blue, Color.Blue, Color.Blue, Color.Blue   //1палубные
                             };

        Color[] fire_color = { Color.AliceBlue,  //неизвестно
                               Color.SeaGreen,   //мимо
                               Color.Orange,     //ранил
                               Color.OrangeRed,  //убил
                               Color.Silver,     //победил
                               Color.AntiqueWhite //ореол
                             };
        public Battleship()
        {
            InitializeComponent();
            user_field = new Redactor();
            user_field.show_ship = show_user_ship;
            user_field.show_fight = show_user_field_fire;
            user_field.show_aureole = show_user_field_fire;

            enemy_field = new Redactor();
            enemy_field.show_ship = show_enemy_ship;
            enemy_field.show_fight = show_enemy_field_fire;
            enemy_field.show_aureole = show_enemy_field_fire;

            init_grid(user_grid);
            init_grid(enemy_grid);

            enemy_grid.ClearSelection();
            user_grid.ClearSelection();
        }

        private void init_grid(DataGridView grid)
        {
            grid.Rows.Clear();
            grid.Columns.Clear();
            grid.DefaultCellStyle.BackColor = field_color;
            for (int i = 0; i < Field.fieldSize.x; i++ )
            {
                grid.Columns.Add("col_" + i.ToString(), abc.Substring(i, 1));
                for (int j = 0; j < Field.fieldSize.y; j++ )
                {
                    grid.Rows.Add();
                    grid.Rows[j].HeaderCell.Value = (j + 1).ToString();
                }
            }
            grid.Height = Field.fieldSize.y * grid.Rows[0].Height + grid.ColumnHeadersHeight;
            grid.ClearSelection();
            grid.ReadOnly = true;
            grid.ClearSelection();
        }

      

        

        private void button2_Click(object sender, EventArgs e)
        {
            user_field.clearField();
            enemy_field.clearField();
            standUserShipsRandom();
            standEnemyShipsRandom();

            
            user_grid.CellMouseClick += rotateShip;
            pictureBox1.AllowDrop = true;
            pictureBox2.AllowDrop = true;
            pictureBox3.AllowDrop = true;
            pictureBox4.AllowDrop = true;
            pictureBox5.AllowDrop = true;
            pictureBox6.AllowDrop = true;
            pictureBox7.AllowDrop = true;
            pictureBox8.AllowDrop = true;
            pictureBox9.AllowDrop = true;
            pictureBox10.AllowDrop = true;
            user_grid.CellMouseDown += dataGridView_CellMouseDown_forMoveShip;
            user_grid.CellMouseEnter += dataGridView_CellMouseEnter_forMovingShip;
            user_grid.CellMouseLeave += dataGridView_CellMouseLeave_forMovingShip;
            user_grid.CellMouseUp += dataGridView_CellMouseUp_forMovingShip;

            enemy_grid.ClearSelection();
            user_grid.ClearSelection();
        }

        private void standUserShipsRandom() 
        {
            user_field.clearField();

            while (user_field.setedShips < Field.summaryShips)
            {
                for (int i = 0; i < Field.summaryShips; i++)
                    if (user_field.isNoShip(i))
                        user_field.stand_random(i);
            }
        }

        private void standEnemyShipsRandom()
        {
            enemy_field.clearField();

            while (enemy_field.setedShips < Field.summaryShips)
            {
                for (int i = 0; i < Field.summaryShips; i++)
                    if (enemy_field.isNoShip(i))
                        enemy_field.stand_random(i);
            }
        }

        private void show_ship(DataGridView grid, Point p, int ships_number) 
        {
            if (ships_number < 0)
                grid[p.x, p.y].Style.BackColor = field_color;
            else
                grid[p.x, p.y].Style.BackColor = ship_color[ships_number];

        }
        private void show_fight(DataGridView grid, Point p, Status status)
        {
                grid[p.x, p.y].Style.BackColor = fire_color[(int)status];
        }

        private void show_user_ship(Point p, int ships_number)
        {
            show_ship(user_grid, p, ships_number);
        }

        private void show_enemy_ship(Point p, int ships_number)
        {
            enemy_grid[p.x, p.y].Style.BackColor = field_color;
        }

        private void show_user_field_fire(Point p, Status status)
        {
            show_fight(user_grid, p, status);
        }


        private void show_user_aureole(Point p, Status status)
        {
            show_fight(user_grid, p, status);
        }

        private void show_enemy_aureole(Point p, Status status)
        {
            show_fight(user_grid, p, status);
        }

        private void show_enemy_field_fire(Point p, Status status)
        {
            show_fight(enemy_grid, p, status);
        }


        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //*******************************************************************************
            
            Point p = new Point(e.ColumnIndex, e.RowIndex);
            Status status = enemy_field.user_fire(p);
            textBox1.Text += System.Environment.NewLine + "user Hit: " + (p.x +1) + "." + (abc[p.y]) + "  " + status.ToString();
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0) 
                enemy_grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Selected = false;
            if (status == Status.wins)
            {
                enemy_grid.CellClick -= dataGridView_CellClick;
                DialogResult dr = MessageBox.Show("You win, try again?", "Message", MessageBoxButtons.YesNo);
                if (dr == System.Windows.Forms.DialogResult.Yes)
                {
                    user_field.clearField();
                    enemy_field.clearField();
                    for (int i = 0; i < Field.fieldSize.x; i++)
                    {
                        for (int j = 0; j < Field.fieldSize.x; j++)
                        {
                            user_grid[i, j].Style.BackColor = field_color;
                            enemy_grid[i, j].Style.BackColor = field_color;
                        }
                    }
                    textBox1.Text = "";
                    return;
                }
                else
                {
                    DialogResult = System.Windows.Forms.DialogResult.Abort;
                    this.Close();
                }
            }

            if (status != Status.wounded && status != Status.killed)
            {
                enemy_grid.CellClick -= dataGridView_CellClick;
                label1.Text = "Ход противника";
                enemy_hiting();
            }
            
        }
        
        private void first_hit(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            button1.Enabled = false;
            user_grid.CellMouseClick -= rotateShip;
            user_grid.CellMouseDown -= dataGridView_CellMouseDown_forMoveShip;
            user_grid.CellMouseEnter -= dataGridView_CellMouseEnter_forMovingShip;
            user_grid.CellMouseLeave -= dataGridView_CellMouseLeave_forMovingShip;
            user_grid.CellMouseUp -= dataGridView_CellMouseUp_forMovingShip;

            Random rnd = new Random();
            int who_hiting = rnd.Next(0, 2);
            if (who_hiting == 0)             // ход игрока
            {
                user_hiting();
            }
            else                            // ход противника
            {
                enemy_hiting();
            }
        }
        

        private void user_hiting()
        { 
             enemy_grid.CellClick += dataGridView_CellClick;
        }


        private void enemy_hiting()
        {
                label1.Text = "Ход противника";
                label1.Refresh();
            while(true)
            {
                
                TimeSpan MaxWait = TimeSpan.FromMilliseconds(1000);
                System.Threading.Thread.Sleep(MaxWait);
                Point p = user_field.pointForFire();
                Status status = user_field.enemy_fire(p);
                textBox1.Text += System.Environment.NewLine + "enemy Hit: " + p.x + "." + p.y + status.ToString();
                if (status == Status.wins)
                {
                    enemy_grid.CellClick -= dataGridView_CellClick;
                    DialogResult dr = MessageBox.Show("You loose, try again?", "Message", MessageBoxButtons.YesNo);
                    if (dr == System.Windows.Forms.DialogResult.Yes)
                    {
                        user_field.clearField();
                        enemy_field.clearField();
                        for (int i = 0; i < Field.fieldSize.x; i++ )
                        {
                            for (int j = 0; j < Field.fieldSize.x; j++)
                            {
                                user_grid[i, j].Style.BackColor = field_color;
                                enemy_grid[i, j].Style.BackColor = field_color;
                            }
                        }
                        textBox1.Text = "";
                        return;
                    }
                    else
                    {
                        DialogResult = System.Windows.Forms.DialogResult.Abort;
                        this.Close();
                    }
                }
                if (status == Status.missed)
                {
                    enemy_grid.CellClick += dataGridView_CellClick;
                    label1.Text = "Ход игрока";
                    break;
                }
                    
            }

        }


        protected void add_ships_manually(object sender, EventArgs e) 
        {
            enemy_field.clearField();
            standEnemyShipsRandom();
            user_field.clearField();
            user_grid.CellMouseClick += rotateShip;
            groupBox1.Visible = true;
            pictureBox1.AllowDrop = true;
            pictureBox2.AllowDrop = true;
            pictureBox3.AllowDrop = true;
            pictureBox4.AllowDrop = true;
            pictureBox5.AllowDrop = true;
            pictureBox6.AllowDrop = true;
            pictureBox7.AllowDrop = true;
            pictureBox8.AllowDrop = true;
            pictureBox9.AllowDrop = true;
            pictureBox10.AllowDrop = true;
            user_grid.CellMouseDown += dataGridView_CellMouseDown_forMoveShip;
            user_grid.CellMouseEnter += dataGridView_CellMouseEnter_forMovingShip;
            user_grid.CellMouseLeave += dataGridView_CellMouseLeave_forMovingShip;
            user_grid.CellMouseUp += dataGridView_CellMouseUp_forMovingShip;
            
        }
        
       

        bool formDragging;
        bool gridDragging;
        System.Drawing.Point startDragPoint;
        Point[] startShipCords;

        void pictureBox_MouseDown(object sender, MouseEventArgs e) 
        {
            //Это гениально!!!
            formDragging = true;
            startDragPoint = e.Location;
            (sender as PictureBox).Parent = this;//(sender as PictureBox).Parent = ((sender as PictureBox).Parent).Parent;
            this.Controls.SetChildIndex((PictureBox)sender, 0);
            textBox1.Text = (sender as PictureBox).Width.ToString();
        }
        Point[] inequalityOfNeighboringСells;
        Point[] lastFilledCells;
        int shipNumber;

        void dataGridView_CellMouseDown_forMoveShip(object sender, DataGridViewCellMouseEventArgs e)
        {
            if(e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                Point clickedCell = new Point(e.ColumnIndex, e.RowIndex);
                if (user_field.shipsMap[clickedCell.x, clickedCell.y] != -1)
                {
                    gridDragging = true;
                    shipNumber = user_field.shipsMap[clickedCell.x, clickedCell.y];
                    startShipCords = user_field.ship[shipNumber].deck;
                    lastFilledCells = user_field.ship[shipNumber].deck;
                    inequalityOfNeighboringСells = new Point[startShipCords.Length];
                    user_field.deleteShip(shipNumber);
                    for (int i = 0; i < startShipCords.Length; i++)
                    {
                        user_grid[startShipCords[i].x, startShipCords[i].y].Style.BackColor = Color.Blue;
                    }
                    for (int i = 0; i < startShipCords.Length; i++)
                    {
                        inequalityOfNeighboringСells[i].x = startShipCords[i].x - clickedCell.x;
                        inequalityOfNeighboringСells[i].y = startShipCords[i].y - clickedCell.y;
                    }
                }
                else
                {
                    return;
                }
            }
        }

        void dataGridView_CellMouseEnter_forMovingShip(object sender, DataGridViewCellEventArgs e)
        {
            if (gridDragging)
            {

                user_grid.CurrentCell.Selected = false;
                Point enteredPoint = new Point(e.ColumnIndex, e.RowIndex);
                Point[] pointsToFill = new Point[startShipCords.Length];
                for (int i = 0; i < startShipCords.Length; i++)
                {
                    pointsToFill[i].x = enteredPoint.x + inequalityOfNeighboringСells[i].x;
                    pointsToFill[i].y = enteredPoint.y + inequalityOfNeighboringСells[i].y;
                }


                for (int i = 0; i < pointsToFill.Length; i++)
                {
                    if (pointsToFill[i].x < 0 || pointsToFill[i].x >= Field.fieldSize.x ||
                        pointsToFill[i].y < 0 || pointsToFill[i].y >= Field.fieldSize.y)
                    {
                        pointsToFill = lastFilledCells;
                        canNotDoIt(pointsToFill);
                        return;
                    }     
                }
                for (int i = 0; i < pointsToFill.Length; i++)
                {
                    if (!pointIsFree(pointsToFill[i]))
                    {
                        for (int j = 0; j < pointsToFill.Length; j++)
                            user_grid[pointsToFill[j].x, pointsToFill[j].y].Style.BackColor = Color.Red;
                        System.Threading.Thread.Sleep(500);
                        for (int j = 0; j < pointsToFill.Length; j++)
                        {
                            if (user_field.shipsMap[pointsToFill[i].x, pointsToFill[i].y] == -1) user_grid[pointsToFill[j].x, pointsToFill[j].y].Style.BackColor = field_color;
                            else user_grid[pointsToFill[j].x, pointsToFill[j].y].Style.BackColor = ship_color[shipNumber];
                        }
                        return;
                    } 
                }
                for (int i = 0; i < pointsToFill.Length; i++) 
                {
                    user_grid[pointsToFill[i].x, pointsToFill[i].y].Style.BackColor = Color.Blue;
                }
                lastFilledCells = pointsToFill;
            }
        }

        void dataGridView_CellMouseLeave_forMovingShip(object sender, DataGridViewCellEventArgs e)
        {
            if (gridDragging) 
            {
                for (int i = 0; i < lastFilledCells.Length; i++)
                {
                    if(user_field.shipsMap[lastFilledCells[i].x, lastFilledCells[i].y] == -1)
                        user_grid[lastFilledCells[i].x, lastFilledCells[i].y].Style.BackColor = field_color;
                    else
                        user_grid[lastFilledCells[i].x, lastFilledCells[i].y].Style.BackColor = ship_color[user_field.shipsMap[lastFilledCells[i].x, lastFilledCells[i].y]];
                }                       
            }
        }

        void dataGridView_CellMouseUp_forMovingShip(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (gridDragging)
            {
                Point upedPoint = new Point(e.ColumnIndex, e.RowIndex);
                Point[] pointsToFill = lastFilledCells;

                for (int i = 0; i < pointsToFill.Length; i++)
                {
                    if (pointsToFill[i].x < 0 || pointsToFill[i].x >= Field.fieldSize.x ||
                        pointsToFill[i].y < 0 || pointsToFill[i].y >= Field.fieldSize.y ||
                        !pointIsFree(pointsToFill[i]))
                    {
                        pointsToFill = startShipCords;
                        user_field.setShip(shipNumber, pointsToFill);
                        gridDragging = false;
                        return;
                    }
                    
                }
                //if (pointsToFill.Equals(startShipCords))
                //{
                //    textBox1.Text += "     equals!";
                //    gridDragging = false;


                //}
                //else
                //{ 
                user_field.setShip(shipNumber, pointsToFill);
                //} 
                gridDragging = false;
            }
        }

        void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (formDragging)
            {
                textBox1.Text = " " + ((PictureBox)sender).Left + ";" + ((PictureBox)sender).Top;
                //Чтобы корабль не вышел за край по горизонтали
                if ((((PictureBox)sender).Left > 0) && (((PictureBox)sender).Left < (this.Width - ((PictureBox)sender).Width)))
                {
                    ((Control)sender).Left = ((Control)sender).Location.X +
                      (e.Location.X - startDragPoint.X);
                }
                else
                {
                    if (((PictureBox)sender).Location.X + (e.Location.X - startDragPoint.X) < 0)
                        ((PictureBox)sender).Left = 0;
                    else
                        if (((PictureBox)sender).Location.X + (e.Location.X - startDragPoint.X) > this.Width - ((Control)sender).Width)
                            ((PictureBox)sender).Left = this.Width - ((PictureBox)sender).Width;
                        else
                            ((Control)sender).Left = ((Control)sender).Location.X +
                              (e.Location.X - startDragPoint.X);
                }

                //Чтобы корабль не вышел за край по вертикали
                if ((((PictureBox)sender).Top > 0) && (((PictureBox)sender).Top < this.Height - ((Control)sender).Height))
                {
                    ((PictureBox)sender).Top = ((PictureBox)sender).Location.Y +
                      (e.Location.Y - startDragPoint.Y);
                }

                else
                {
                    if (((PictureBox)sender).Location.Y + (e.Location.Y - startDragPoint.Y) < 0)
                        ((PictureBox)sender).Top = 0;
                    else
                        if (((PictureBox)sender).Location.Y + (e.Location.Y - startDragPoint.Y) > this.Height - ((Control)sender).Height)
                            ((PictureBox)sender).Top = this.Height - ((PictureBox)sender).Height;
                        else
                            ((PictureBox)sender).Top = ((PictureBox)sender).Location.Y +
                              (e.Location.Y - startDragPoint.Y);
                }

                if ((sender as PictureBox).Top > (user_grid.Location.Y + user_grid.ColumnHeadersHeight) &&
                    (sender as PictureBox).Top < (user_grid.Location.Y + user_grid.Height) &&
                    (sender as PictureBox).Left > (user_grid.Location.X + user_grid.RowHeadersWidth) &&
                    (sender as PictureBox).Left < (user_grid.Location.X + user_grid.Width))
                {
                    DataGridView.HitTestInfo cellUnderShipTopLeft = user_grid.HitTest(((sender as PictureBox).Location.X - user_grid.Location.X), ((sender as PictureBox).Location.Y - user_grid.Location.Y));
                    Point p = new Point(cellUnderShipTopLeft.ColumnIndex, cellUnderShipTopLeft.RowIndex);
                    int shipLength;
                    switch ((sender as PictureBox).Name)
                    {
                        case "pictureBox1": shipLength = 4;
                            break;
                        case "pictureBox2": shipLength = 3;
                            break;
                        case "pictureBox3": shipLength = 3;
                            break;
                        case "pictureBox4": shipLength = 2;
                            break;
                        case "pictureBox5": shipLength = 2;
                            break;
                        case "pictureBox6": shipLength = 2;
                            break;
                        default: shipLength = 1;
                            break;
                    }
                    if (p.x + shipLength <= Field.fieldSize.x && p.x >= 0 && p.y >= 0)                     //если правый край корабля не выходит за пределы поля
                    {
                        for (int i = 0; i < shipLength; i++ )
                        {
                            Point innerPoint = new Point(p.x + i, p.y);
                            if (!pointIsFree(innerPoint))
                            { 
                                (sender as PictureBox).Image = SeaBattle.Properties.Resources.rect4redBorder;
                                return;
                            }
                        }
                        (sender as PictureBox).Image = SeaBattle.Properties.Resources.rect4;
                    }
                    else
                    {
                        (sender as PictureBox).Image = SeaBattle.Properties.Resources.rect4redBorder;
                    }
                }
            
            }
        }

        bool pointIsFree(Point p)
        {
            Point innerP = new Point(p.x, p.y);
            for (innerP.x = p.x - 1; innerP.x <= p.x + 1; innerP.x++)
            {
                for (innerP.y = p.y - 1; innerP.y <= p.y + 1; innerP.y++)
                {
                    if (!user_field.onField(innerP))
                        continue;
                    if (user_field.shipsMap[innerP.x, innerP.y] != -1)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private void canNotDoIt(Point[] deck) 
        {
            for (int j = 0; j < deck.Length; j++)
            {
                user_grid[deck[j].x, deck[j].y].Style.BackColor = Color.Red;

            }
            System.Threading.Thread.Sleep(1000);
            for (int j = 0; j < deck.Length; j++)
            {
                user_grid[deck[j].x, deck[j].y].Style.BackColor = Color.Blue;
            }
            return;
        }

        void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            (sender as PictureBox).Image = SeaBattle.Properties.Resources.rect4;
            if (formDragging)
            {
                if ((sender as PictureBox).Top > (user_grid.Location.Y + user_grid.ColumnHeadersHeight) &&
                    (sender as PictureBox).Top < (user_grid.Location.Y + user_grid.Height) &&
                    (sender as PictureBox).Left > (user_grid.Location.X + user_grid.RowHeadersWidth) &&
                    (sender as PictureBox).Left < (user_grid.Location.X + user_grid.Width))
                {
                    DataGridView.HitTestInfo cellUnderShipTopLeft = user_grid.HitTest(((sender as PictureBox).Location.X - user_grid.Location.X), ((sender as PictureBox).Location.Y - user_grid.Location.Y));
                    Point p = new Point(cellUnderShipTopLeft.ColumnIndex, cellUnderShipTopLeft.RowIndex);
                    int shipLength;
                    int shipNumber;
                    switch ((sender as PictureBox).Name) 
                    {
                        case "pictureBox1": shipLength = 4; shipNumber = 0;
                            break;
                        case "pictureBox2": shipLength = 3; shipNumber = 1;
                            break;
                        case "pictureBox3": shipLength = 3; shipNumber = 2;
                            break;
                        case "pictureBox4": shipLength = 2; shipNumber = 3;
                            break;
                        case "pictureBox5": shipLength = 2; shipNumber = 4;
                            break;
                        case "pictureBox6": shipLength = 2; shipNumber = 5;
                            break;
                        case "pictureBox7": shipLength = 1; shipNumber = 6;
                            break;
                        case "pictureBox8": shipLength = 1; shipNumber = 7;
                            break;
                        case "pictureBox9": shipLength = 1; shipNumber = 8;
                            break;
                        default: shipLength = 1; shipNumber = 9;
                            break;
                    }
                    
                    if (p.x + shipLength <= Field.fieldSize.x && p.x >= 0 && p.y >=0)    //если правый край корабля по иксу не выходит за пределы поля
                    {
                        for (int i = 0; i < shipLength; i++)
                        {
                            Point innerPoint = new Point(p.x+i, p.y);

                            if (!pointIsFree(innerPoint))
                            {
                                formDragging = false;
                                (sender as PictureBox).Parent = groupBox1;
                                (sender as PictureBox).Location = startDragPoint;
                                return;
                            }
                                
                        }
                        //set the ship and del picBox
                        Point[] deck = new Point[shipLength];
                        for (int j = 0; j < shipLength; j++)
                        {
                            deck[j] = new Point(p.x + j, p.y);
                            textBox1.Text += "   " + (p.x + j) + ";" + p.y;
                        }

                        user_field.setShip(shipNumber, deck);
                        (sender as PictureBox).Dispose();
                    }
                }
                else
                {
                    (sender as PictureBox).Parent = groupBox1;
                    (sender as PictureBox).Location = startDragPoint;
                }
                formDragging = false;
            }
        }


        private void rotateShip(object sender, DataGridViewCellMouseEventArgs e)
        {
            gridDragging = false;
            user_field.setShip(shipNumber, startShipCords);
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                Point p = new Point(e.ColumnIndex, e.RowIndex);
                if (user_field.shipsMap[p.x, p.y] == -1)
                    return;
                else
                {

                    if (user_field.ship[shipNumber].deck.Length == 1)
                    {
                        return;
                    }
                    if (user_field.ship[shipNumber].deck[0].x == user_field.ship[shipNumber].deck[1].x) //если корабль вертикально расположен
                    {
                        Point[] sourceShipLocation = user_field.ship[shipNumber].deck;
                        if (sourceShipLocation.Length == 1) return;
                        Point[] possibleShipLocation = new Point[user_field.ship[shipNumber].deck.Length];
                        possibleShipLocation[0] = user_field.ship[shipNumber].deck[0];
                        if (user_field.ship[shipNumber].deck[0].x + user_field.ship[shipNumber].deck.Length > Field.fieldSize.x)
                        {
                            canNotDoIt(sourceShipLocation);
                            return;
                        }
                        for (int i = 1; i < possibleShipLocation.Length; i++)
                        {
                            possibleShipLocation[i] = new Point(user_field.ship[shipNumber].deck[0].x + i, user_field.ship[shipNumber].deck[0].y);
                        }
                        user_field.deleteShip(shipNumber);
                        for (int i = 0; i < sourceShipLocation.Length; i++)
                        {
                            if (!pointIsFree(possibleShipLocation[i]))
                            {
                                user_field.setShip(shipNumber, sourceShipLocation);
                                canNotDoIt(sourceShipLocation);
                            }
                        }
                        user_field.setShip(shipNumber, possibleShipLocation);

                    }
                    else                                                                                     //если корабль горизонтально расположен
                    {
                        Point[] sourceShipLocation = user_field.ship[shipNumber].deck;
                        if (sourceShipLocation.Length == 1) return;
                        Point[] possibleShipLocation = new Point[user_field.ship[shipNumber].deck.Length];
                        possibleShipLocation[0] = user_field.ship[shipNumber].deck[0];
                        if (user_field.ship[shipNumber].deck[0].y + user_field.ship[shipNumber].deck.Length > Field.fieldSize.y)
                        {
                            canNotDoIt(sourceShipLocation);
                            return;
                        }
                        for (int i = 1; i < possibleShipLocation.Length; i++)
                        {
                            possibleShipLocation[i] = new Point(user_field.ship[shipNumber].deck[0].x, user_field.ship[shipNumber].deck[0].y + i);
                        }
                        user_field.deleteShip(shipNumber);
                        for (int i = 0; i < sourceShipLocation.Length; i++)
                        {
                            if (!pointIsFree(possibleShipLocation[i]))
                            {
                                user_field.setShip(shipNumber, sourceShipLocation);
                                canNotDoIt(sourceShipLocation);
                                return;
                            }
                        }
                        user_field.setShip(shipNumber, possibleShipLocation);

                    }
                }
            }
        }
    }
}
