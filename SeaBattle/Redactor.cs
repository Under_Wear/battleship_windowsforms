﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeaBattle
{
    class Redactor : Field
    {
        static int[] ships_widht = { 4, 3, 3, 2, 2, 2, 1, 1, 1, 1};
        public static Random random = new Random();

        public Redactor() 
            :base()
        {
            
        }

       

        public void clearField()        //очистить поле
        {
            for (int i = 0; i < fieldSize.x; i++)
            {
                for (int j = 0; j < fieldSize.y; j++)
                {
                    shipsMap[i, j] = -1;
                    show_ship(new Point(i, j), -1);

                    hitsMap[i, j] = Status.unnown;
                    show_fight(new Point(i, j), Status.unnown);
                }
            }
            for (int i = 0; i < summaryShips; i++)
            {
                ship[i] = null;
            }
            setedShips = 0;
            killedShips = 0;
        }


        public void setShip(int shipsNumber, Point[] deck)   //установить корабль
        { 
            if (ship[shipsNumber] != null)
                deleteShip(shipsNumber);

            ship[shipsNumber] = new Ship(deck);

            foreach (Point p in deck)
            {
                shipsMap[p.x, p.y] = shipsNumber;
                show_ship(p, shipsNumber);
            }
            setedShips++;
        }

        public void deleteShip(int shipsNumber)              // убрать корабль
        {
            foreach (Point p in ship[shipsNumber].deck)
            {
                shipsMap[p.x, p.y] = -1;
                show_ship(p, -1);
            }
            ship[shipsNumber] = null;
            setedShips--;
        }


        public bool isNoShip(int shipsNumber)        //нет ли в точке корабля
        {
            return ship[shipsNumber] == null;
        }


        protected void clear_point(Point p)          //очистить точку
        {
            if (!onField(p))
                return;
            if (shipsMap[p.x, p.y] == -1)
                return;
            deleteShip(shipsMap[p.x, p.y]);
        }

        protected void clear_area(Point p)          //очистить площадь вокруг точки
        {
            Point innerP;

            for (innerP.x = p.x - 1; innerP.x <= p.x + 1; innerP.x++ ) 
                for (innerP.y = p.y - 1; innerP.y <= p.y + 1; innerP.y++)
                    clear_point(innerP);
            
        }

        public bool stand_random(int ship_number)
        {
            int ship_width = ships_widht[ship_number];
            Point ship_start_point;
            Point step_displacement;

            if (random.Next(2) == 0)  //по вертикали
            {
                ship_start_point = new Point(random.Next(0, fieldSize.x - ship_width + 1), random.Next(0, fieldSize.y));
                step_displacement = new Point(1, 0);
            }
            else                      // по горизонтали
            {
                ship_start_point = new Point(random.Next(0, fieldSize.x), random.Next(0, fieldSize.y - ship_width + 1));
                step_displacement = new Point(0, 1);
            }

            Point [] deck = new Point[ship_width];

            for (int i = 0; i < ship_width; i++)
            {
                deck[i] = new Point(ship_start_point.x + i * step_displacement.x, ship_start_point.y + i * step_displacement.y);
                clear_area(deck[i]);
            }
            setShip(ship_number, deck);
                return true;
        }


        public bool standShipByUser(int ship_number, Point startPoint, Point endPoint)
        {
            int ship_width = ships_widht[ship_number];
            Point ship_start_point;
            Point step_displacement;

            if (random.Next(2) == 0)  //по вертикали
            {
                ship_start_point = new Point(random.Next(0, fieldSize.x - ship_width + 1), random.Next(0, fieldSize.y));
                step_displacement = new Point(1, 0);
            }
            else                      // по горизонтали
            {
                ship_start_point = new Point(random.Next(0, fieldSize.x), random.Next(0, fieldSize.y - ship_width + 1));
                step_displacement = new Point(0, 1);
            }

            Point[] deck = new Point[ship_width];

            for (int i = 0; i < ship_width; i++)
            {
                deck[i] = new Point(ship_start_point.x + i * step_displacement.x, ship_start_point.y + i * step_displacement.y);
                clear_area(deck[i]);
            }
            setShip(ship_number, deck);
            return true;
        }
       

        

    }
}
 